# Node Buffer Refresher

## Description
This is a refresher for personal reference on the subject of buffers in the context of NodeJS.

### Processing and/or completion date(s)
June 20, 2020

## Creative Motivation
The first search engine result that popped out at me on the subject of buffers was this write-up at stackabuse called [Encoding and Decoding Base-64 Strings in NodeJS](https://stackabuse.com/encoding-and-decoding-base64-strings-in-node-js/ "link to stackabuse"). I, then, adjusted my personal code base and wanted to share my findings, too.

## This is one of many baby steps to cure personal shortfalls in deciphering Sequelize errors
I have in recent weeks attempted to complete the 
[ApolloGraphQL fullstack tutorial](https://www.apollographql.com/docs/tutorial/introduction/ "link to tutorial"). There was a node buffer statement in the content of the exercise and, frankly, I did not recognize it. I was used to the older format for those buffer statement expressions. Thus, I decided to brush up on the subject of NodeJS buffers, experiment and share. 

Please help me if you are able. Thank you for your visit. 


## God bless!