// #!/usr/bin/env node

"use strict";

const path = require("path");
const fs = require("fs");

let imagePath = path.join( __dirname, 'test-image-in', 'real_men_love_jesus.png');
let buff = fs.readFileSync( imagePath );
let basesixtyfourdata = buff.toString('base64');

console.log(`Image converted to base 64 is: \n\n ${basesixtyfourdata}\n\n That's all!` );

